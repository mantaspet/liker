import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SuggestionsModule } from './suggestions/suggestions.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SuggestionsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
