import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';

/**
 * Nothing to test here for now, but since services will eventually need to be tested
 * and Angular CLI by default generates a spec file for a service there is no need to discard it
 */
describe('UsersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
});
