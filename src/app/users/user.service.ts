import { Injectable } from '@angular/core';
import { User } from './user';

/**
 * I chose to implement a dummy user service because the task description indicates
 * that in future synchronization between users will be required. Doing it this way
 * instead of an constant variable somewhere in the app prevents unnecessary refactoring later down the line
 * because the service can be expanded as needed while the retrieval of the current user stays the same.
 * I do this to avoid potential errors in the future, and it doesn't take any extra time
 * because of Angular CLI's code scaffolding
 */

@Injectable({
  providedIn: 'root'
})
export class UserService {
  protected currentUser: User = {
    id: 1
  };

  constructor() { }

  getCurrentUser(): User {
    return this.currentUser;
  }
}
