import { Component, OnInit, ViewChild } from '@angular/core';
import { SuggestionService } from '../suggestion.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './suggestion-form.component.html',
  styleUrls: ['./suggestion-form.component.css']
})
export class SuggestionFormComponent implements OnInit {
  @ViewChild('suggestionForm') suggestionForm: NgForm;
  suggestionTitle: string;

  constructor(public suggestionService: SuggestionService) { }

  ngOnInit() {
  }

  onSubmit(): void {
    if (this.suggestionForm.valid) {
      this.suggestionService.createSuggestion(this.suggestionTitle);
      this.suggestionForm.reset();
    } else {
      alert('Please enter a suggestion title');
    }
  }
}
