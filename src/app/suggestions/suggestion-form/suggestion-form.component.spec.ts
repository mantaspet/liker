import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionFormComponent } from './suggestion-form.component';
import { SuggestionsModule } from '../suggestions.module';
import { TestSuggestionService } from '../../../testing/test-suggestion.service';
import { SuggestionService } from '../suggestion.service';
import { FormsModule } from '@angular/forms';

let fixture: ComponentFixture<SuggestionFormComponent>;

describe('SuggestionFormComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SuggestionsModule, FormsModule],
      providers: [
        { provide: SuggestionService, useClass: TestSuggestionService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionFormComponent);
    fixture.detectChanges();
  });

  it('should display an alert dialog when user submits an empty form', () => {
    spyOn(window, 'alert');
    fixture.whenStable().then( () => {
      fixture.componentInstance.onSubmit();
      expect(window.alert).toHaveBeenCalled();
    });
  });

  it('should call method createSuggestion in SuggestionService and clear form upon submit', () => {
    spyOn(fixture.componentInstance.suggestionService, 'createSuggestion');
    fixture.whenStable().then( () => {
      fixture.componentInstance.suggestionForm.controls['title'].setValue('Brand new suggestion');
      fixture.componentInstance.onSubmit();
      expect(fixture.componentInstance.suggestionService.createSuggestion).toHaveBeenCalled();
      expect(fixture.componentInstance.suggestionTitle).toBe(null);
    });
  });
});
