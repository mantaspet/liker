import { TestBed } from '@angular/core/testing';

import { SuggestionService } from './suggestion.service';

/**
 * I didn't implement tests for suggestion service since it's methods are mocked
 * and those tests would change once proper implementation is done.
 * The current app work flow is covered by component unit test and e2e test suites anyway.
 */
describe('SuggestionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuggestionService = TestBed.get(SuggestionService);
    expect(service).toBeTruthy();
  });
});
