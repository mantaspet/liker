import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Suggestion } from './suggestion';
import { UserService } from '../users/user.service';
import { getTestSuggestions } from '../../testing/test-suggestions';

@Injectable({
  providedIn: 'root'
})
export class SuggestionService {
  /**
   * I'm storing data inside the service's private BehaviorSubject and expose access to it via a readonly Observable stream because
   * this way the service data is encapsulated and can't be overridden
   * and it can be conveniently consumed with the async pipe in components (example in suggestions-table.component.html)
   * and it remains as a single source of truth that will always be kept in sync in all components that require this data.
   */
  protected _suggestions: BehaviorSubject<Suggestion[]> = new BehaviorSubject([]);
  readonly suggestions$: Observable<Suggestion[]> = this._suggestions.asObservable();

  constructor(protected userService: UserService) {}

  getSuggestions(): void {
    // To avoid code duplication, I import the data that is used for testing while there is no API
    this._suggestions.next(getTestSuggestions());
  }

  createSuggestion(title: string): void {
    const suggestions = this._suggestions.getValue();
    const nextId = suggestions[suggestions.length - 1].id + 1;
    const newSuggestion = {
      id: nextId,
      title: title,
      votedBy: []
    };
    suggestions.push(newSuggestion);
    this._suggestions.next(suggestions);
  }

  addVote(suggestion: Suggestion): void {
    const suggestions = this._suggestions.getValue();
    const index = suggestions.findIndex(s => s.id === suggestion.id);
    suggestions[index].votedBy.push(this.userService.getCurrentUser().id);
    this._suggestions.next(suggestions);
  }

  removeVote(suggestion: Suggestion): void {
    const suggestions = this._suggestions.getValue();
    const suggestionIndex = suggestions.findIndex(s => s.id === suggestion.id);
    const voteIndex = suggestions[suggestionIndex].votedBy.indexOf(this.userService.getCurrentUser().id);
    suggestions[suggestionIndex].votedBy.splice(voteIndex, 1);
    this._suggestions.next(suggestions);
  }
}
