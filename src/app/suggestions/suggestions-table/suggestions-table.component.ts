import { Component, OnInit } from '@angular/core';
import { SuggestionService } from '../suggestion.service';
import { Suggestion } from '../suggestion';
import { UserService } from '../../users/user.service';

@Component({
  selector: 'app-suggestions-table',
  templateUrl: './suggestions-table.component.html',
  styleUrls: ['./suggestions-table.component.css']
})
export class SuggestionsTableComponent implements OnInit {
  constructor(
    public suggestionService: SuggestionService,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.suggestionService.getSuggestions();
  }

  toggleVote(suggestion: Suggestion): void {
    if (suggestion.votedBy.indexOf(1) > -1) {
      this.suggestionService.removeVote(suggestion);
    } else {
      this.suggestionService.addVote(suggestion);
    }
  }
}
