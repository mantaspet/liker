import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionsTableComponent } from './suggestions-table.component';
import { SuggestionsModule } from '../suggestions.module';
import { TestSuggestionService } from '../../../testing/test-suggestion.service';
import { SuggestionService } from '../suggestion.service';
import { getTestSuggestions } from '../../../testing/test-suggestions';

const SUGGESTIONS = getTestSuggestions();
let fixture: ComponentFixture<SuggestionsTableComponent>;
let page: Page;

describe('SuggestionsTableComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SuggestionsModule],
      providers: [
        { provide: SuggestionService, useClass: TestSuggestionService }
      ]
    })
    .compileComponents()
    .then(createComponent);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionsTableComponent);
    fixture.detectChanges();
  });

  it('should display suggestions', () => {
    expect(page.suggestionRows.length).toBeGreaterThan(0);
  });

  it('1st suggestion should match 1st test suggestion', () => {
    const expectedSuggestion = SUGGESTIONS[0];
    const actualSuggestion = page.suggestionRows[0].textContent;
    expect(actualSuggestion).toContain(expectedSuggestion.id.toString(), 'suggestion.id');
    expect(actualSuggestion).toContain(expectedSuggestion.title, 'suggestion.title');
    expect(actualSuggestion).toContain(expectedSuggestion.votedBy.length.toString(), 'suggestion.voteCount');
  });

  it('should contain a like icon on suggestion liked by current user', () => {
    expect(page.suggestionRows[1].innerHTML).toContain('img');
  });

  it('calls addVote method in SuggestionService when user clicks a suggestion that he already voted for', () => {
    spyOn(fixture.componentInstance.suggestionService, 'addVote');
    page.suggestionRows[0].querySelectorAll('button')[0].click();
    expect(fixture.componentInstance.suggestionService.addVote).toHaveBeenCalled();
  });

  it('calls removeVote method in SuggestionService when user clicks a suggestion that he has not voted for', () => {
    spyOn(fixture.componentInstance.suggestionService, 'removeVote');
    page.suggestionRows[1].querySelectorAll('button')[0].click();
    expect(fixture.componentInstance.suggestionService.removeVote).toHaveBeenCalled();
  });
});

function createComponent() {
  fixture = TestBed.createComponent(SuggestionsTableComponent);

  fixture.detectChanges();

  return fixture.whenStable().then(() => {
    fixture.detectChanges();
    page = new Page();
  });
}

class Page {
  suggestionRows: HTMLTableRowElement[];

  constructor() {
    const suggestionRowNodes = fixture.nativeElement.querySelectorAll('tr.suggestion-row');
    this.suggestionRows = Array.from(suggestionRowNodes);
  }
}
