import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuggestionsTableComponent } from './suggestions-table/suggestions-table.component';
import { SuggestionFormComponent } from './suggestion-form/suggestion-form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    SuggestionsTableComponent,
    SuggestionFormComponent
  ],
  declarations: [
    SuggestionsTableComponent,
    SuggestionFormComponent
  ]
})
export class SuggestionsModule { }
