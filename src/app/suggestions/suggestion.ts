export class Suggestion {
  id: number;
  title: string;
  votedBy: number[]; // array of user IDs who have voted for this suggestion
}
