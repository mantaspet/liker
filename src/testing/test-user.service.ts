import { Injectable } from '@angular/core';
import { User } from '../app/users/user';
import { UserService } from '../app/users/user.service';

@Injectable({
  providedIn: 'root'
})
export class TestUserService extends UserService {
  constructor() {
    super();
    this.currentUser = {
      id: 1
    };
  }

  getCurrentUser(): User {
    return this.currentUser;
  }
}
