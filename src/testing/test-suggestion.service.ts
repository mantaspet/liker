import { Injectable } from '@angular/core';

import { getTestSuggestions } from './test-suggestions';
import { SuggestionService } from '../app/suggestions/suggestion.service';
import { Suggestion } from '../app/suggestions/suggestion';
import { TestUserService } from './test-user.service';

@Injectable()
export class TestSuggestionService extends SuggestionService {

  constructor() {
    super(new TestUserService());
  }

  getSuggestions(): void {
    this._suggestions.next(getTestSuggestions());
  }

  createSuggestion(title: string): void {}

  addVote(suggestion: Suggestion): void {}

  removeVote(suggestion: Suggestion): void {}
}
