import { Suggestion } from '../app/suggestions/suggestion';

export function getTestSuggestions(): Suggestion[] {
  return [
    { id: 1, title: 'Item 1', votedBy: [2] },
    { id: 2, title: 'Item 2', votedBy: [1, 2, 3, 4, 5] },
    { id: 3, title: 'Item 3', votedBy: [2] },
    { id: 4, title: 'Item 4', votedBy: [] },
  ];
}
