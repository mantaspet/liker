## Note
There are comments in services and service spec files where I explain my reasoning behind related decisions. The rest of the code is not commented much because I tend to use semantic variable names and 'dumb' self explanatory code whenever possible and only add comments in places where I feel an explanation is needed.

# Liker

Real-time web application, where users can share the list of their own suggestions and vote for best one.  

### UI Components

- **Suggestions table**

    List of current suggestions, where each suggestion row contains votes counter.

    Clicking on votes counter user adds or removes his own vote for current suggestion

    User can vote for single suggestion only once

    When user adds his vote for suggestions, "like" symbol appears near the counter

- **Suggestion form**

    Simple form, containing text input and submit button, where user can add his own suggestions

### Tasks

Build simple UI, where user can add, preview or vote for his own suggestions (like a todo list).
No synchronization between users is required at the moment

## Setup

Run `npm install` to install dependencies

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
