import { browser, by, element } from 'protractor';

describe('Suggestions page', () => {
  it('should add a newly created suggestion to suggestions table', () => {
    browser.get('/');
    element(by.id('suggestion-input')).sendKeys('Brand new suggestion with a totally unique name');
    element(by.id('add-button')).click();
    expect(element(by.id('suggestions-table')).getText()).toContain('Brand new suggestion with a totally unique name');
  });
});
